\documentclass{lug}
\usepackage{tikz-cd}
\usepackage{parskip}
\usepackage{bbm}
\usepackage{nicematrix}
%\usepackage[backend=bibtex, sorting=none, style=numeric-comp, defernumbers]{biblatex}


\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\C}{\mathbb{C}}

\newcommand{\sep}{\quad \Rightarrow \quad}
\newcommand{\Mod}[1]{\ (\text{mod}\ #1)}
\newcommand{\Div}[1]{\ \text{div}\ #1}
\newcommand{\dif}{\, \textrm{d}}

\renewcommand{\vec}{\mathbf}

\newcommand{\op}[1]{\langle #1\rangle}
\newcommand{\floor}[1]{\left \lfloor #1 \right \rfloor }
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil }
\newcommand{\bigo}{\mathcal{O}}
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\newcommand{\M}{\mathbb{M}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\lcm}{\mathrm{lcm}}
\newcommand{\tr}{\mathrm{tr} }
\newcommand{\Inn}{\mathrm{Inn}}

\newcommand{\id}{\mathbbm{1}}
%\renewcommand{\ker}{\ \mathrm{ker} \ }


\title{Tensors}
\author{Joseph}
\def\gitlocation{\url{https://gitlab.com/josephmckinsey/bells-theorem-notes}}

\begin{document}

% Emphasize coordinate free notion.
% We will care about the algebraic properties.
\begin{frame}{A few linear algebra notions}
  We will only consider vector spaces over a fixed field $k$, and $V$ and $W$
  will always be vector spaces over $k$.
  \begin{definition}
    A \textbf{linear map} is a function $f : V \to W$ such that
    $f(v_1 + v_2) = f(v_1) + f(v_2)$ and $f(c v) = c f(v)$ for any $c \in k$.
  \end{definition}

  \begin{block}{Some notation considerations}
    \begin{itemize}
      \item Whenever possible, I will avoid using arrows on vectors (i.e. $v$ is a vector).
      \item Usually indexed vectors represent basis vectors: $v_i$.
      \item Very occasionally, $v_i$ will represent the vector
      with components $v_1, v_2, \cdots$.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{A few more linear algebra notions}
  % This is just a convenient set that happens to be a vector space.
  % If necessary, write out an example with $x = c_1 v_1 + \cdots + c_n v_n$.
  \begin{definition}
    Given a vector space $V$, there is a \textbf{dual} vector space of linear
    functionals $V^* = V \to k$ (linear maps to $k$).
  \end{definition}
  Given a basis of $V$, say $v_1, \cdots, v_n$, we can construct
  a basis $\{f_i\}$ of $V^*$:
  \begin{align*}
    f_i(v_1) = 0, \cdots, f_i(v_i) = 1, \cdots, f_i(v_n) = 0 \\
    \text{ or } f_i(v_j) = \delta_{i j} \text{ where } \delta_{i j} = \begin{cases}
      1 & \text{ if } i = j \\
      0 & \text{ otherwise}
    \end{cases}
  \end{align*}
  % Isomorphism extends $v_i \mapsto f_i$.
  So a choice of basis implies a straightforward isomorphism between $V$ and $V^*$. Usually this linear functional is a
  ``row vector'', and the transpose is this isomorphism.
\end{frame}

\begin{frame}{Bilinear functions}
  \begin{definition}
    A \textbf{bilinear map} is a function $f : V_1 \times V_2 \to W$ such that
    $f(v_1, \cdot)$ is linear and $f(\cdot, v_2)$ is linear for all $v_1 \in V_1$
    and $v_2 \in V_2$.
  \end{definition}
  \begin{align*}
    f(c v_1, v_2) = c f(v_1, v_2) \\
    f(v_1, c v_2) = c f(v_1, v_2) \\
    f(c v_1, c v_2) = c^2 f(v_1, v_2)
  \end{align*}
\end{frame}

\begin{frame}{Examples bilinear functions}
  \begin{example}
  Multiplication!
  \begin{align*}
    f(x, y) = xy
  \end{align*}
  This works for real numbers, complex numbers, polynomials!
  \end{example}
\end{frame}

\begin{frame}{Examples of bilinear functions}
  % Super simple example is $f(1, 2) = 1 \cdot 2$
  \begin{example}[Quadratic forms and Inner Products]
    A \textbf{quadratic form} on $V$ is a bilinear function
    $f : V \times V \to k$.

    When $k = \R$, $f(v, w) = f(w, v)$ and $f(v, v) \geq 0$ with equality when $v = 0$, then $f$ is an \textbf{inner product}.
  \end{example}

  % Remind that $V \to V$ is a vector space too
  \begin{example}[``Outer Products'']
    There is a bilinear map $e : V \times V^* \to (V \to V)$ given by
    \begin{align*}
      e(v, f)(w) = f(w) v
    \end{align*}
    If we look at $V \to V$ as the $n \times n$ matrices $M_{n}$, then
    $e(v, w^T) = v w^T$.
  \end{example}
\end{frame}

\begin{frame}{Examples of bilinear functions from calculus}
  \begin{example}[Higher-order Derivatives]
    The derivative of a function $f : \R^m \to \R^n$ at a point $x$ is a linear
    function $(D f)(x) : \R^m \to \R^n$. \\

    The derivative of $D f$ at $x$ is another linear map
    $(D^2 f)(x) : \R^m \to (\R^m \to \R^n)$. Uncurrying:
    $(D^2 f)(x) : \R^m \times \R^m \to \R^n$ is a bilinear
    map. \\

    If $f$ has continuous second derivatives, then $(D^2 f)(x)$
    is symmetric since $\frac{\partial^2 f}{\partial x \partial y} = \frac{\partial^2 f}{\partial y \partial x}$.
  \end{example}
\end{frame}

\begin{frame}{Examples from the matrices}
  \begin{example}[Determinant on $2\times 2$]
    The determinant over $2 \times 2$ matrices can be seen as a bilinear function
    \begin{align*}
    \det{\begin{bmatrix} v \\ w \end{bmatrix}} =
    \det{\begin{bmatrix} v_{1} & v_{2} \\ w_{1} & w_{2} \end{bmatrix}} =
    v_{1} w_{2} - v_{2} w_{1}
    \end{align*}
  \end{example}
  Determinants are an important and special class of multilinear functions
  over rows or columns.
\end{frame}

% Mention that this works for any $k$-algebra (product + vector space).
\begin{frame}{More examples from matrices}
  \begin{example}[Matrix multiplication]
    Matrix multiplication is bilinear:
    \begin{align*}
      f : M_n \times M_n \to M_n
    \end{align*}
  \end{example}
  \begin{align*}
    f((c_1 A + c_2 B), C)  = c_1 A C + c_2 B C = c_1 f(A, C) + c_2 f(B, C) \\
    f(A, c_1 B + c_2 C)  = c_1 A B + c_2 A C = c_1 f(A, B) + c_2 f(A, C)
  \end{align*}
\end{frame}

\begin{frame}{This one is one to remember for later}
  \begin{example}[Outer product again]
    There is a bilinear map $e : V \times V^* \to (V \to V)$ given by
    \begin{align*}
      e(v, f)(w) = f(w) v
    \end{align*}
    If we look at $V \to V$ as the $n \times n$ matrices $M_{n}$, then
    $e(v, w^T) = v w^T$.
  \end{example}
\end{frame}

% Now we'll focus on $V \times V \to k$.
\begin{frame}{How do we study bilinear functions?}
  \begin{center}
  Linear algebra of course!
  \end{center}

  Since you can linearly combine bilinear functions $f : V \times V \to k$,
  $V \times V \to k$ is a vector space. A natural question is what's the basis:
  \begin{alertblock}{Exercise}
    Given a basis of $V$, $v_1, \cdots, v_n$ find a basis of
    the bilinear functions on $V \times V \to k$.
  \end{alertblock}
\end{frame}

\begin{frame}{Solution to Exercise}
  \begin{alertblock}{Exercise}
    Given a basis of $V$, $v_1, \cdots, v_n$ find a basis of
    the bilinear functions on $V \times V \to k$.
  \end{alertblock}

  Since
  \begin{align*}
  f(c_1 v_1 + \cdots + c_n v_n, d_1 v_1 + \cdots + d_n v_n)
   = \\ c_1 d_1 f(v_1, v_1) + c_1 d_2 f(v_1, v_2) + \cdots + c_n d_n f(v_n, v_n),
  \end{align*}
  any bilinear function $f : V \times V \to k$ is
  defined by its action on elements of the form $(v_i, v_j)$.
  In fact it is uniquely defined.
\end{frame}
\begin{frame}{Solution to Exercise}
  \begin{alertblock}{Exercise}
    Given a basis of $V$, $v_1, \cdots, v_n$ find a basis of
    the bilinear functions on $V \times V \to k$.
  \end{alertblock}

  % I'm setting $f(v_i, v_j) = 1$ and all others to $0$.
  \only<1>{
    Define $\phi_{i j}$ by
    \begin{align*}
      \phi_{i j}(c_1 v_1 + \cdots + c_n v_n, d_1 v_1 + \cdots + d_n v_n) = c_i d_j.
    \end{align*}
    $\phi_{i j}$ is bilinear in each argument, and these form a basis
    of bilinear functions on $V \times V \to k$.
  }
  \only<2>{
    We can also define $\phi_{i j}$ using the dual basis $f_i$ on $V^*$,
    then $\phi_{i j}(v, w) = f_i(v) f_j(w)$.
  }
\end{frame}

\begin{frame}{A few notes on the exercise}
  \begin{block}{Basis of $V \times V \to k$}
  Define $\phi_{i j}$ by
  \begin{align*}
    \phi_{i j}(c_1 v_1 + \cdots c_n v_n, d_1 v_1 + \cdots d_n v_n) = c_i d_j.
  \end{align*}
  $\phi_{i j}$ form a basis
  of bilinear functions on $V \times V \to k$.
  \end{block}

  \begin{itemize}
  \item We had to use a basis of $V$ just like we did for $V^*$, so it's not very \textit{natural}.
  \item Can we avoid this?
  \item Clue: We ended up using the combination of all basis elements $(v_i, v_j)$.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Universal Property and the Tensor Product}
  \begin{block}{Idea}
    We're going to push all that construction of a product basis into a new
    space vector space $V \otimes V$ and in fact we can make more general, so we
    will look at $V \otimes W$.
  \end{block}
  Categorically, we want $V \otimes W$ to allow us to
  replace any bilinear map $f : V \times W \to Z$ with a
  unique linear map $\bar{f} : V \otimes W \to Z$.
  \begin{center}
      % https://tikzcd.yichuanshen.de/#N4Igdg9gJgpgziAXAbVABwnAlgFyxMJZABgBoBGAXVJADcBDAGwFcYkQA1AAgB0e8AtvC4cQAX1LpMufIRTlSxanSat2AdXGSQGbHgJEySmgxZtEnXjwiDhoscphQA5vCKgAZgCcIApGRAcCCQFEEZ6ACMYRgAFaX05EC8sZwALHBATVXMQDy1PHz9EAKCkACYss3Y+NFSsfNzC8ppSxFDTNQs+GAAPLDgcOABCKwj6L2APMUywrDAcqHo4VKdxSjEgA
    \begin{tikzcd}
    V \otimes W \arrow[blue, r, "\exists! \bar{f}", dashed] & Z \\
    V \times W \arrow[red, ru, "f"'] \arrow[red, u, "\phi"]    &
    \end{tikzcd}
  \end{center}

  \only<1>{
    Given any linear map $\bar{f}$, we automatically get a map $f = \bar{f} \circ \phi$.
  }
  \only<2>{
    Since this is just a universal property, \textit{the tensor product is
    only defined up to isomorphism}.
  }
\end{frame}

\begin{frame}{Relating this to $V \times W \to k$}
  Since we can always get $f : V \times W \to Z$ by looking at projections
  $f_i : V \times W \to k$ where $f_i$ is $f$ projected onto the $i$th basis vector $Z$,
  we can construct $V \otimes W$ just with $Z = k$.

  We already showed that $f : V \times V \to k$ is uniquely defined by its
  action on $(v_i, v_j)$, and our basis is essentially the same:

  % Using the tensor product
  \begin{block}{Basis of $V \times W$}
    Define $v \otimes w := \phi(v, w)$. These are \textbf{pure tensors} (or simple tensors).

    If $\{v_i\}_{i \in I}$ is a basis of $V$ and $\{w_j\}_{j \in J}$ is a basis
    of $W$, then $\{v_i \otimes w_j\}_{(i, j) \in I \times J}$ is a basis of
    $V \otimes W$.
  \end{block}
\end{frame}

\begin{frame}{Laws of $V \otimes W$}
  \only<1>{Using bilinearity of $\phi$, we have}
  \only<1-2>{
  \begin{itemize}
  \item $(c v) \otimes w = c (v \otimes w) = v \otimes (c w)$,
  \item $(u + v) \otimes w = u \otimes w + v \otimes w$, and
  \item $u \otimes (v + w) = u \otimes v + u \otimes w$.
  \end{itemize}
  }
  \only<1>{
  \begin{align*}
    1 \otimes 2 = 2 \otimes 1
  \end{align*}
  }

  \only<1>{In fact, this supplies an alternate definition}
  \only<2-3>{
  \begin{definition}[Equivalence Class Definition]
    \[
    V \otimes W = \frac{F(V \times W)}{U}
    \]
    where $F(\cdot)$ is the free vector space and $U$ is generated by
    $(c v, w) = c (v, w)$, $(v, c w) = c (v, w)$, $(u + v, w) - (u, w) - (v, w)$, and $(u, v + w) - (u, v) + (u, w)$.
  \end{definition}
  }
  \only<3>{
    \begin{alertblock}{Perverse}
    I find this definition perverse since it doesn't give you
    more than $\phi$ does, while having this huge vector space $F(V \times W)$.
    \end{alertblock}
  }
\end{frame}

\begin{frame}{Transformation laws of $V \otimes W$}
  \begin{definition}[More tensor products]
  Let $f_1 : V \to V'$ and $f_2 : W \to W'$ be two linear maps, then
  we have a tensor product $f_1 \otimes f_2 : V \otimes W \to V' \otimes W'$.
  \end{definition}

  \only<1>{
    We can define this since
    $\phi \circ (f_1 \times_{\mathrm{Set}} f_2)$ is a bilinear map (prove!).
    Then the universal property gives $f_1 \otimes f_2$.
  }
  \only<2>{
  \begin{example}[Tensoring two linear maps]
    Given $f(c_1 v_1 + c_2 v_2) = c_2 v_1 + c_1 v_2$ or
    $f v = \begin{bmatrix} 0 & 1 \\ 1 & 0 \end{bmatrix} v$.

    Then $(\id \otimes f)(v_i \otimes v_1) = v_i \otimes v_2$ and
    $(\id \otimes f)(v_i \otimes v_2) = v_i \otimes v_1$.
  \end{example}
  }
  \only<3>{
  \begin{example}[Tensoring two linear maps]
    Given $f(c_1 v_1 + c_2 v_2) = c_2 v_1 + c_1 v_2$ or
    $f v = \begin{bmatrix} 0 & 1 \\ 1 & 0 \end{bmatrix} v$.

    Then using the ordered basis $((v_1, v_1), (v_1, v_2), (v_2, v_1), (v_2, v_2))$, we can describe $\id \otimes f$ as a matrix:
    \begin{align}
      \id \otimes f =
      \begin{bNiceArray}{CCCC}[last-col, code-for-last-col = \color{red}\small]
      0 & 1 & 0 & 0 & v_1 \otimes v_1 \\
      1 & 0 & 0 & 0 & v_1 \otimes v_2 \\
      0 & 0 & 0 & 1 & v_2 \otimes v_1 \\
      0 & 0 & 1 & 0 & v_2 \otimes v_2
    \end{bNiceArray}
    \end{align}
    \textbf{Easy Exercise: } write the matrix for $f \times \id$ under the same basis.
  \end{example}
  }
\end{frame}

\begin{frame}{The MATLAB point of view}
  \begin{definition}[Kronecker product]
    Let $A \in M_{k \times l}$ and $B_{m \times n}$ be two matrices.
    The kronecker product of $A$ and $B$ is the
    matrix $A \otimes B$ where $(A \otimes B)_{(m i_1 + i_2) (n j_1 + j_2)} = A_{i_1 j_1} B_{i_2 j_2}$
    \[
      \mathbf {A} \otimes \mathbf {B} ={\begin{bmatrix}A_{11}\mathbf {B} &\cdots &A_{1l}\mathbf {B} \\\vdots &\ddots &\vdots \\A_{k1}\mathbf {B} &\cdots &A_{kl}\mathbf {B} \end{bmatrix}}
    \]

    \only<1>{
      This effectively is tensor product of $A \otimes B$ and the two are related via a fixed basis with the dictionary ordering $(v_1 \otimes v_1, v_1 \otimes v_2, v_2 \otimes v_1, v_2 \otimes v_2)$.
    }
  \end{definition}
  \only<2>{
  \begin{alertblock}{Caution}
    Using the kronecker product hides all the tensor structure,
    so there are usually better options (often much more efficient!).
  \end{alertblock}
  }
  \only<3>{
    \begin{definition}[The MATLAB definition of tensor product]
      A vector of length $m$ and a vector of length $n$ can combine into
      a vector of length $m n$ with the kronecker product %$\vec{v} \otimes \vec{w}$.
    \end{definition}
  }
\end{frame}

\begin{frame}{Matrices get big}
  \begin{block}{}
    Kronecker gets too big. How do we compute?
  \end{block}

  Recall that I mentioned that $v_i$ could also represent the component in the
  $i$th basis vector...
\end{frame}

\begin{frame}{Basic Einstein Sum Notation}
  Suppose we are multiplying two matrices $A$ and $B$ together,
  then we can write $(A B)_{i k} = \sum_j A_{i j} B_{j k}$.

  We will suppress the $\sum$ and rely on repeated indices to sum.

  \begin{example}
    \vspace{-0.5cm}
    \begin{align*}
      v \otimes w \qquad &\text{ is the same as } \qquad v_i w_j \\
      A \otimes B \qquad &\text{ is the same as } \qquad A_{ij} B_{kl} \\
      (A \otimes B) (v \otimes w) \qquad &\text{ is the same as } \qquad A_{ij} B_{kl} v_j w_l = (A_{i j} v_j)(B_{kl} w_l) \\
      \tr{A} \qquad &\text{ is the same as } \qquad A_{ii}
    \end{align*}
  \end{example}
  This is available in \texttt{numpy}'s \texttt{einsum}.
\end{frame}

\begin{frame}{Pitfalls of Einstein Sum Notation}
  \begin{alertblock}{Oh no!}
    The repeated sum
    \begin{align*}
      \tr (v \otimes w) = v_{i} w_i
    \end{align*}
    would allow us to define trace on any tensor product $V \otimes V$.
    This is \textbf{not} well-defined, and in fact will cause
    serious problems for even physicists.
  \end{alertblock}
  The Physicist's solution is to only combine lower indices with
  higher indices, so $(AB)^i_k = A^i_j B^j_k$. More on this later.

  The mathematician's solution is not forget about the structure of what we're doing.
\end{frame}

\begin{frame}{Tensors as multidimensional arrays}
  We have already seen how we can use the product of two bases.

  This suggests that we organize elements of $V \otimes V$ into the structure of a multidimensional array \texttt{a[i][j]}.

  \begin{itemize}[<+->]
  \item But now you've forgotten some of the structure of your source spaces ($V \times V$ is not a linear map and not really a matrix you can multiply with).
  \item It gets really annoying to visualize past $2$ dimensions.
  \item There is usually a more compact factorization.
  \item The shoe don't fit: past $\mathrm{deg}\ 2$ basic stuff like SVD breaks down.
  \end{itemize}
\end{frame}

\begin{frame}{Remember the outer product?}
    There is a bilinear map $e : V \times V^* \to (V \to V)$ given by
    \begin{align*}
      e(v, f)(w) = f(w) v
    \end{align*}

    When we upgrade this to a linear map $e : V \otimes V^* \to (V \to V)$,
    we get an \textbf{isomorphism} since $T_{i j}(v) = v_i f_j(v)$ form a basis.
    (They are the matrices with a $1$ in row $i$ column $j$ and a $0$ elsewhere).

    \only<2>{
      \begin{exampleblock}{Physicists Einstein Notation}
        \vspace{-0.5cm}
        \begin{align*}
        A^{i_1 \cdots i_m}_{j_1 \cdots j_m} \in \overbrace{V \otimes \cdots \otimes V}^{i_1, \cdots, i_n} \otimes \overbrace{V^* \otimes \cdots \otimes V^*}^{j_1, \cdots, j_n}
        \end{align*}
      \end{exampleblock}
    }
\end{frame}

% f \otimes g \mapsto (v \otimes w \mapsto f v \otimes g w) is an isomorphism

\begin{frame}{A lot of things are tensors! Remember the inner product?}
  Everything is tensors.
  \begin{align*}
    V \otimes V \to \R &\cong (V \otimes V)^* \\
                       &\cong V^* \otimes V^* \quad \text{(prove!)}\\
                       &\cong V \to V^*
  \end{align*}
  So a quadratic form gives a way to convert $V$ to $V^*$.
  The nondegenerate inner product then make this an isomorphism.
\end{frame}

\begin{frame}{All those bilinear maps are tensors!}
  \begin{itemize}
    \item Higher-order derivatives
    \item Determinants are tensors $V^* \otimes \cdots \otimes V^*$
    and in fact are the unique alternating tensors of rank $n$ (that's for later).
    \item Matrix multiplication is a tensor?
    \item Real numbers are tensors, technically.
    \item Polynomials in multiple variables are tensors.
    \item And more\ldots
  \end{itemize}
\end{frame}

\begin{frame}{Physical examples of tensors}
  \begin{example}[Cauchy Stress Tensor $\sigma_{i j}$]

    \only<1>{
      \begin{center}
      \includegraphics[width=0.5\textwidth]{graphics/components_stress_tensor}
      \end{center}
    }
    \only<2>{
      \begin{center}
      \includegraphics[width=0.5\textwidth]{graphics/stress_squeeze}
      \end{center}
      If we cut at a point $x$ with a surface $S$, then we can
      look at the traction vector $\vec{T}^{(n)} = \frac{dF}{dS}$.
      We are \textbf{not} looking at the force, but how the force
      changes as the size of surface changes.
    }
    \only<3>{
      \begin{center}
      \includegraphics[width=0.5\textwidth]{graphics/stress_squeeze}
      \end{center}
      As you might suspect $\vec{T}^{(n)} = \frac{dF}{dS}$ defines
      a linear map from the normal vector to a traction vector:
      $\vec{T}^{(n)}_j = \sum_i \sigma_{i j} n_i$. This is usually
      viewed as a second order tensor at every point (normal vectors
      transform differently).
    }
    \only<4>{
      \begin{center}
      \includegraphics[width=0.5\textwidth]{graphics/components_stress_tensor}
      \end{center}
      Formally, the normal vector $n$ should be an oriented surface element (hence why transforming $V$ can break things),
      but that is for another talk.
    }
  \end{example}
\end{frame}

\begin{frame}{Other physical examples}
  \begin{itemize}
    \item The quantum mechanical state of $n$ particles (next lecture) lives in the tensor product of the $2$ particle states.
    \item The electricity and magnetism combine into an alternating tensor field (a $2$-form).
    \item A Riemannian metric is a real inner product at all points in a space, and so a tensor field.
    \item The curvature of spacetime is a tensor.
  \end{itemize}
\end{frame}

\begin{frame}{Other topics I will not get to}
  \begin{itemize}
    \item A diagrammatic calculus for tensors
    \item Alternating tensors (I've already mentioned them). This is the proper way to formulate cross products, Stokes' theorem, normal vectors, electromagnetism, and others.
    \item Symplectic forms which allow you to generalize a lot of properties of complex spaces.
    \item Tensor decompositions which let you multiply matrices faster than $n^3$ as well as do machine learning (Netflix recommendation algorithm).
  \end{itemize}
\end{frame}

\begin{frame}[t, allowframebreaks]{References}
  \nocite{*}
\bibliographystyle{IEEEtran}
\bibliography{tensors}
\end{frame}

\end{document}

% Local Variables:
% TeX-engine: default
% TeX-command-extra-options: "-shell-escape"
% End:
