\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{lug}[2017/10/18]

\LoadClass{beamer}

%numbering=none
\usetheme[numbering=none, progressbar=frametitle,block=fill]{Berlin}
\setbeamercovered{dynamic}
\RequirePackage{graphicx}

\RequirePackage{ifxetex}
\ifxetex\RequirePackage{fontspec}\fi

\RequirePackage{minted}
\RequirePackage{hyperref}

\renewcommand*\footnoterule{}
\setminted{autogobble,python3,mathescape}

\beamertemplatenavigationsymbolsempty%

\setlength\parindent{0pt}

\AtBeginDocument{%
    \begin{frame}
      \maketitle

      \gitlocation
    \end{frame}
}
