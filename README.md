# Presentation on Tensors and Bell's Theorem

- [Tensors](tensors.pdf)


- [Bell's Theorem](bellstheorem.pdf)


- [Pluto notebook with calculations](pluto_notebook/bells_theorem.jl)
