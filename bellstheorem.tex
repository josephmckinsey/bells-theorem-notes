\documentclass{lug}
\usepackage{tikz-cd}
\usepackage{parskip}
\usepackage{bbm}
\usepackage{nicematrix}
\usepackage{physics}
%\usepackage[backend=bibtex, sorting=none, style=numeric-comp, defernumbers]{biblatex}


\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\C}{\mathbb{C}}

\newcommand{\sep}{\quad \Rightarrow \quad}
\newcommand{\Mod}[1]{\ (\text{mod}\ #1)}
\newcommand{\Div}[1]{\ \text{div}\ #1}
\newcommand{\dif}{\, \textrm{d}}

\renewcommand{\vec}{\mathbf}

%\newcommand{\op}[1]{\langle #1\rangle}
\newcommand{\floor}[1]{\left \lfloor #1 \right \rfloor }
\newcommand{\ceil}[1]{\left \lceil #1 \right \rceil }
%\newcommand{\bigo}{\mathcal{O}}
%\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

\newcommand{\M}{\mathbb{M}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\lcm}{\mathrm{lcm}}
\newcommand{\Inn}{\mathrm{Inn}}

\newcommand{\id}{\mathbbm{1}}
%\renewcommand{\ker}{\ \mathrm{ker} \ }

\title{Bell's Theorem}
\author{Joseph}
\def\gitlocation{\url{https://gitlab.com/josephmckinsey/bells-theorem-notes}}

\begin{document}

\begin{frame}{Bell's Theorem: The Setup}
  \begin{enumerate}
    \item We'll start with two electrons entangled in a particular state
    \item Alice will get one electron and choose one of two measurements $A_0$ and $A_1$ randomly.
    \item Bob will get one electron and choose one of two measurements $B_0$ and $B_1$ randomly.
    \item They will combine their measurements their measurements
          using $\expval{A_0 B_0} + \expval{A_0 B_1} + \expval{A_1 B_0} - \expval{A_1 B_1}$.
  \end{enumerate}
  $A_0, B_0, A_1, B_1$ will all take values in $-1, 1$.
\end{frame}

\begin{frame}{Well, classically...}
  If we consider $A_0, B_0, A_1, B_1$ as random variables,
  then
  \begin{align*}
    |\text{Expectation}| &= |\expval{A_0 B_0} + \expval{A_0 B_1} + \expval{A_1 B_0} - \expval{A_1 B_1}| \\
                         &= |\expval{A_0 B_0 + A_0 B_1 + A_1 B_0 - A_1 A_1}| \\
                         &= |\expval{A_0 (B_0 + B_1) + A_1 (B_0 - B_1)}| \\
                           &\leq 2
  \end{align*}
  Since if $B_0 = B_1$, then $B_0 + B_1 = \pm 2$ and $B_0 - B_1 = 0$. While if $B_0 \neq B_1$, then $B_0 + B_1 = 0$ and $B_0 - B_1 = \pm 2$.
\end{frame}

\begin{frame}{Bra-ket notation}
  \begin{definition}[Bra-ket notation]
    An element $v \in V$ is written $\ket{v}$.

    A linear functional $f \in V^*$ is written as $\bra{f}$.
  \end{definition}

  \begin{enumerate}
    \item $\bra{f} \ket{v} = f(v)$ (an ``inner product'')
    \item $\ket{v} \bra{f}$ is the ``outer product'': $(\ket{v} \bra{f}) w = f(w) v$.
    \item $\bra{f} \ket{ A v} = \matrixel{f}{A}{v}$.
  \end{enumerate}
\end{frame}

\begin{frame}{Hilbert Space}
  \begin{definition}[Hilbert Space]
    A \textbf{Hilbert space} $\mathcal{H}$ is a complete inner product space.

    \begin{itemize}
    \item \textbf{Complete} means that when a sequence eventually gets arbitrarily close together, it converges.

    \item \textbf{Inner product space} is a vector space $V$ with an inner product $\langle \cdot, \cdot \rangle$ which
    is linear in the first argument and antilinear in the second.
    \end{itemize}
  \end{definition}
  \begin{example}
    All finite dimensional spaces with any inner product (including
    the usual dot product $\langle \phi, \psi \rangle = \psi^* \phi$)
  \end{example}
\end{frame}

\begin{frame}{Facts about Hilbert Spaces}
  \begin{block}{Inner products make vectors into functionals}
    \vspace{-0.5cm}
    \begin{align*}
      f_w : v \mapsto \langle v, w \rangle
    \end{align*}
    Note that this is linear in $v$ and antilinear in $w$.
  \end{block}
  % Riesz-rep proved using Hilbert projection theorem
  % That's proved using Cauchy criterion + convexity + closure + dumb inequalities.
  \begin{block}{All (continuous) functionals come from vectors (Riesz representation theorem)}
    Since we can take any vector and get a functional (and vice-versa),
    we can treat all our kets like bras and vice versa.
    \begin{align*}
      f_w : \ket{v} \mapsto \bra{w}\ket{v}
    \end{align*}
  \end{block}
\end{frame}

\begin{frame}{Recall a tensor}
  Given two vector spaces, we can get the tensor product $V \otimes W$
  plus a bilinear map $\phi : V \times W \to V \otimes W$ which factors
  all other bilinear maps.

  \begin{block}{Bra-ket notation}
    \[ \ket{v} \otimes \ket{w} = \ket{v w} \]
    It is common to also just have numbers as the vectors, so $\ket{01}$.
  \end{block}
\end{frame}

\begin{frame}{Tensor of two Hilbert spaces}
  Given two Hilbert spaces $(\mathcal{H}_1, \langle , \rangle_1)$ and $(\mathcal{H}_2, \langle, \rangle_2)$, we can define an inner product on $\mathcal{H}_1 \otimes \mathcal{H}_2$.
  \begin{align*}
    \langle v_1 \otimes v_2, w_1 \otimes w_2 \rangle = \langle v_1, w_1 \rangle_1 \langle v_2, w_2 \rangle_2
  \end{align*}

  We can then extend this linearly to $\mathcal{H}_1 \otimes \mathcal{H}_2$.

  \only<1>{
  \begin{alertblock}{Exercise}
    Prove $\langle, \rangle$ is an inner product.
  \end{alertblock}

  \begin{alertblock}{Wait! Did we ``complete'' it?}
    Now our nice (Cauchy) sequences might not converge.

    $\mathcal{H}_1 \otimes \mathcal{H}_2$ will now be the
    completion of the tensor product.
  \end{alertblock}
  }
  \only<2>{
    \begin{block}{Universal Property Construction?}
      We could have also used a universal property,
      but the category would take far longer to describe.
    \end{block}
  }
\end{frame}

\begin{frame}{It's time for Quantum Mechanics}
  For simplicity, I'm going to assume that all finite-dimensional
  Hilbert spaces $\mathcal{H}$ are finite-dimensional.
\end{frame}

\begin{frame}{Ingredients of Quantum Mechanics}
  \begin{enumerate}
  \item Each particle $i$ will have a state in some Hilbert space $\mathcal{H}_i$.

  This gives us \textbf{superposition}.
  \only<1>{
          \begin{center}

\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt

\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
%uncomment if require: \path (0,300); %set diagram left start at 0, and has height of 300

%Straight Lines [id:da4251346552082831]
\draw    (100,122) -- (209.65,69.22) ;
\draw [shift={(211.45,68.35)}, rotate = 154.29] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da03733417316323551]
\draw    (100,122) -- (215.54,86.93) ;
\draw [shift={(217.45,86.35)}, rotate = 163.12] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da7292289619407336]
\draw    (100,122) -- (217.47,105.63) ;
\draw [shift={(219.45,105.35)}, rotate = 172.06] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da8347863897235656]
\draw    (100,122) -- (219.45,129.23) ;
\draw [shift={(221.45,129.35)}, rotate = 183.46] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da6200620622016108]
\draw    (100,122) -- (215.51,150.87) ;
\draw [shift={(217.45,151.35)}, rotate = 194.03] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da6573444144376893]
\draw    (100,122) -- (209.62,169.55) ;
\draw [shift={(211.45,170.35)}, rotate = 203.45] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

% Text Node
\draw (131,63.4) node [anchor=north west][inner sep=0.75pt]    {$e^{-}$};


\end{tikzpicture}
\end{center}
}


  \only<2-3>{
  \item The full system will live in the tensor product space $\mathcal{H}_1 \otimes \cdots \otimes \mathcal{H}_n$.

  This gives us \textbf{entanglement}.
  }
  \only<2>{
    \begin{center}


\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt

\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
%uncomment if require: \path (0,300); %set diagram left start at 0, and has height of 300

%Straight Lines [id:da4251346552082831]
\draw [color={rgb, 255:red, 208; green, 2; blue, 27 }  ,draw opacity=1 ]   (100,122) -- (209.65,69.22) ;
\draw [shift={(211.45,68.35)}, rotate = 154.29] [color={rgb, 255:red, 208; green, 2; blue, 27 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da03733417316323551]
\draw [color={rgb, 255:red, 245; green, 166; blue, 35 }  ,draw opacity=1 ]   (100,122) -- (215.54,86.93) ;
\draw [shift={(217.45,86.35)}, rotate = 163.12] [color={rgb, 255:red, 245; green, 166; blue, 35 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da7292289619407336]
\draw [color={rgb, 255:red, 126; green, 211; blue, 33 }  ,draw opacity=1 ]   (100,122) -- (217.47,105.63) ;
\draw [shift={(219.45,105.35)}, rotate = 172.06] [color={rgb, 255:red, 126; green, 211; blue, 33 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da8347863897235656]
\draw [color={rgb, 255:red, 74; green, 144; blue, 226 }  ,draw opacity=1 ]   (100,122) -- (219.45,129.23) ;
\draw [shift={(221.45,129.35)}, rotate = 183.46] [color={rgb, 255:red, 74; green, 144; blue, 226 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da6200620622016108]
\draw [color={rgb, 255:red, 189; green, 16; blue, 224 }  ,draw opacity=1 ]   (100,122) -- (215.51,150.87) ;
\draw [shift={(217.45,151.35)}, rotate = 194.03] [color={rgb, 255:red, 189; green, 16; blue, 224 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da6573444144376893]
\draw [color={rgb, 255:red, 144; green, 19; blue, 254 }  ,draw opacity=1 ]   (100,122) -- (209.62,169.55) ;
\draw [shift={(211.45,170.35)}, rotate = 203.45] [color={rgb, 255:red, 144; green, 19; blue, 254 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da5698372451643327]
\draw [color={rgb, 255:red, 144; green, 19; blue, 254 }  ,draw opacity=1 ]   (100,122) -- (14.24,79.24) ;
\draw [shift={(12.45,78.35)}, rotate = 26.5] [color={rgb, 255:red, 144; green, 19; blue, 254 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da6281670298869709]
\draw [color={rgb, 255:red, 208; green, 2; blue, 27 }  ,draw opacity=1 ]   (100,122) -- (15.23,165.44) ;
\draw [shift={(13.45,166.35)}, rotate = 332.87] [color={rgb, 255:red, 208; green, 2; blue, 27 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da22667487312806678]
\draw [color={rgb, 255:red, 245; green, 166; blue, 35 }  ,draw opacity=1 ]   (100,122) -- (6.38,147.82) ;
\draw [shift={(4.45,148.35)}, rotate = 344.58] [color={rgb, 255:red, 245; green, 166; blue, 35 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da5644420826445538]
\draw [color={rgb, 255:red, 126; green, 211; blue, 33 }  ,draw opacity=1 ]   (100,122) -- (7.44,132.13) ;
\draw [shift={(5.45,132.35)}, rotate = 353.75] [color={rgb, 255:red, 126; green, 211; blue, 33 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da7593707278998053]
\draw [color={rgb, 255:red, 74; green, 144; blue, 226 }  ,draw opacity=1 ]   (100,122) -- (3.45,118.42) ;
\draw [shift={(1.45,118.35)}, rotate = 2.12] [color={rgb, 255:red, 74; green, 144; blue, 226 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da11408262246569578]
\draw [color={rgb, 255:red, 189; green, 16; blue, 224 }  ,draw opacity=1 ]   (100,122) -- (7.39,98.84) ;
\draw [shift={(5.45,98.35)}, rotate = 14.04] [color={rgb, 255:red, 189; green, 16; blue, 224 }  ,draw opacity=1 ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

% Text Node
\draw (131,63.4) node [anchor=north west][inner sep=0.75pt]    {$e^{-}$};


\end{tikzpicture}

\end{center}
      }
    \only<3>{
    \item Measurements will correspond to self-adjoint (Hermitian) operators.

    \begin{enumerate}
      \item ``real valued'' measurements
      \item an Uncertainty Principle for \textbf{non-commuting} measurements
            (Robertson–Schrödinger uncertainty relations)
    \end{enumerate}
    }

  \end{enumerate}
\end{frame}

\begin{frame}{How do I measure anything?}
  \begin{definition}[Expectation]
  \only<1>{
  We can get an ``expectation'' (sometimes called a trace)
  of a measurement $A$ on a normalized vector $\psi \in \mathcal{H}$ with
  $\langle \psi, A \psi \rangle = \langle A \psi, \psi \rangle$. \\
  }

  In physics notation: $\expval{A}$ or $\expval{A}{\psi}$.
  \end{definition}

  \only<2>{
    Why is this expectation?
  }
  \only<3>{

  We can diagonalize $A$ into \textit{real} eigenvalues $\lambda_i$ and \textit{orthogonal} eigenvectors $\ket{\psi_i}$.

  Let $\ket{\psi} = \sum_i c_i \ket{\psi_i}$.

  Probability of ``measuring'' $\psi_i$ is $|\bra{\psi_i}\ket{\psi}|^2 = |c_i|^2$ (\textbf{Born's Rule}).

  Then $\expval{A}{\psi} = |c_i|^2 \lambda_i$, so it's kind of like an expectation.
  }

  \only<4>{
    \begin{block}{Scale and phase is unobservable}
      The vectors $\ket{\psi} \in \mathcal{H}$ must be normalized. \\

      Since $\expval{A}{\xi \psi} = \xi \bar{\xi} \expval{A}{\psi}$,
      when $\xi = e^{i \theta}$, it can't be observed.

      Thus we can't detect any scaling $c \ket{\psi}$.
    \end{block}
  }
  \only<5>{
    \begin{block}{Spectral Theorem}
      You can turn a family of commuting self-adjoint operators into
      probability space and random variables for each operator. \\

      \tiny{Terms and conditions apply.}
    \end{block}
  }
\end{frame}

\begin{frame}{With our powers combined}
When we combine \textbf{superposition}, \textbf{entanglement}, and \textbf{non-commuting measurements}, we get spooky things.

\begin{block}{Bell-like Theorems (Ambiguously)}
  There exists states $\psi$ and operators $A_i$ that
  whose expectations have no ``classical'' analog using random variables.
\end{block}
\end{frame}

\begin{frame}{Bell's Theorem: The Setup}
  \begin{enumerate}
    \item We'll start with two electrons entangled in a particular state
    \item Alice will get one electron and choose one of two measurements $A_0$ and $A_1$ randomly.
    \item Bob will get one electron and choose one of two measurements $B_0$ and $B_1$ randomly.
    \item They will combine their measurements their measurements
          using $\expval{A_0 B_0} + \expval{A_0 B_1} + \expval{A_1 B_0} - \expval{A_1 B_1}$.
  \end{enumerate}
\end{frame}

\begin{frame}{Electron Setup}
  \begin{definition}[``Spin'']
  We will represent the ``spin'' of an electron with a vector in $\C^2$
  with basis vectors $\ket{0}$ and $\ket{1}$.
  \end{definition}

  \only<1>{Why? Well that's just how the algebra of electron stuff works. something something relativity.}

  \only<2>{
    The two particle start lives in $\C^2 \otimes \C^2$, with basis
    vectors $\ket{00}, \ket{01}, \ket{10}, \ket{11}$.

    We'll finagle our electrons to have the state
    \begin{align*}
      \ket{\psi} = \frac{1}{\sqrt{2}} (\ket{01} - \ket{10})
    \end{align*}
  }
\end{frame}

\begin{frame}{What does it mean to measure ``spin''?}
  We have three ways to measure spin (i.e. $3$ Hermitian matrices).
  These are the Pauli matrices.
  \begin{align*}
    \sigma_x &= \begin{bmatrix}
      0 & 1 \\
      1 & 0
    \end{bmatrix} \\
    \sigma_y &= \begin{bmatrix}
      0 & -i \\
      i & 0
    \end{bmatrix} \\
    \sigma_z &= \begin{bmatrix}
      1 & 0 \\
      0 & -1
    \end{bmatrix}
  \end{align*}
  We really only care about how these matrices interact (read: multiply).
  But this choice is convenient for its eigenvalues.
\end{frame}


\begin{frame}{Bell's Theorem: The Setup}
  \begin{enumerate}
    \item We'll start with two electrons $\ket{\psi} = \frac{1}{\sqrt{2}} (\ket{01} - \ket{10})$.
    \item Alice will get one electron and choose one of two measurements $A_0$ and $A_1$ randomly.
    \item Bob will get one electron and choose one of two measurements $B_0$ and $B_1$ randomly.
    \item They will combine their measurements their measurements
          using $\expval{A_0 B_0} + \expval{A_0 B_1} + \expval{A_1 B_0} - \expval{A_1 B_1}$.
  \end{enumerate}
\end{frame}

\begin{frame}{What are the measurements Alice and Bob are using?}
  Alice chooses randomly between $A_0$ and $A_1$:
  \begin{align*}
    A_0 = \sigma_z = \begin{bmatrix} 1 & 0 \\ 0 & -1 \end{bmatrix}; \quad
    A_1 = \sigma_x = \begin{bmatrix} 0 & 1 \\ 1 & 0 \end{bmatrix}
  \end{align*}
  Bob chooses between $B_0$ and $B_1$:
  \begin{align*}
    B_0 = - \frac{\sigma_x + \sigma_z}{\sqrt{2}} = \frac{1}{\sqrt{2}} \begin{bmatrix} -1 & -1 \\ -1 & 1 \end{bmatrix}; \quad
    B_1 = \frac{\sigma_x - \sigma_z}{\sqrt{2}} = \frac{1}{\sqrt{2}} \begin{bmatrix} -1 & 1 \\ 1 & 1 \end{bmatrix}
  \end{align*}
\end{frame}


\begin{frame}{Bell's Theorem: The Setup}
  \begin{enumerate}
    \item We'll start with two electrons $\ket{\psi} = \frac{1}{\sqrt{2}} (\ket{01} - \ket{10})$.
    \item Alice will get one electron and choose one of two measurements $A_0 = \sigma_z$ and $A_1 = \sigma_x$ randomly.
    \item Bob will get one electron and choose one of two measurements $B_0 = -\frac{\sigma_x + \sigma_z}{\sqrt{2}}$ and $B_1 = \frac{\sigma_x - \sigma_z}{\sqrt{2}}$ randomly.
    \item They will combine their measurements their measurements
          using $\expval{A_0 B_0} + \expval{A_0 B_1} + \expval{A_1 B_0} - \expval{A_1 B_1}$.
  \end{enumerate}
\end{frame}

\begin{frame}{How to combine the measurements?}
  \begin{definition}[Multiplying two measurements]
    $\expval{A \otimes B} = \expval{A B}$ comes from multiplying the
    results from measuring $A$ and measuring $B$.

    Prove: expand the eigenvalues and eigenvectors of $A \otimes B$
    (this will also show that the product is self-adjoint).
  \end{definition}
  We're going to subtract the product of the measurements when Alice chooses $A_1$ and Bob chooses $B_1$ and add otherwise.
  \begin{align*}
    \expval{A_0 B_0} + \expval{A_0 B_1} + \expval{A_1 B_0} - \expval{A_1 B_1}
  \end{align*}
\end{frame}

\begin{frame}{Bell's Theorem: The Setup}
  \begin{enumerate}
    \item We'll start with two electrons $\ket{\psi} = \frac{1}{\sqrt{2}} (\ket{01} - \ket{10})$.
    \item Alice will get one electron and choose one of two measurements $A_0 = \sigma_z$ and $A_1 = \sigma_x$ randomly.
    \item Bob will get one electron and choose one of two measurements $B_0 = -\frac{\sigma_x + \sigma_z}{\sqrt{2}}$ and $B_1 = \frac{\sigma_x - \sigma_z}{\sqrt{2}}$ randomly.
    \item They will combine their measurements their measurements
          using $\expval{A_0 B_0} + \expval{A_0 B_1} + \expval{A_1 B_0} - \expval{A_1 B_1}$.
  \end{enumerate}
\end{frame}

\begin{frame}{Calculating...}
  \begin{align*}
    \text{Expectation} &= \expval{A_0 B_0} + \expval{A_0 B_1} + \expval{A_1 B_0} - \expval{A_1 B_1} \\
                       &= \matrixel{\psi}{A_0 B_0 + A_0 B_1 + A_1 B_0 - A_1 B_1}{\psi} \\
                       &= \matrixel{\psi}{A_0 (B_0 + B_1) + A_1 (B_0 - B_1)}{\psi} \\
                       &= \frac{2}{\sqrt{2}} \matrixel{\psi}{-\sigma_z \sigma_z - \sigma_x \sigma_x}{\psi}   \end{align*}
  \only<1>{
  \begin{align*}
    \sigma_z \otimes \sigma_z &= \begin{bmatrix}
      1 & 0 \\ 0 & -1
    \end{bmatrix} \otimes \begin{bmatrix}
      1 & 0 \\ 0 & -1 \end{bmatrix} \\
    \sigma_z \otimes \sigma_z &= \begin{bmatrix}
      1 & 0 & 0 & 0 \\
      0 & -1 & 0 & 0 \\
      0 & 0 & -1 & 0 \\
      0 & 0 & 0 & 1
      \end{bmatrix}
  \end{align*}
}
\only<2>{
  \begin{align*}
    \sigma_x \otimes \sigma_x &= \begin{bmatrix}
      0 & 1 \\ 1 & 0
    \end{bmatrix} \otimes \begin{bmatrix}
      0 & 1 \\ 1 & 0
    \end{bmatrix} \\
    \sigma_x \otimes \sigma_x &= \begin{bmatrix}
      0 & 0 & 0 & 1 \\
      0 & 0 & 1 & 0 \\
      0 & 1 & 0 & 0 \\
      1 & 0 & 0 & 0
      \end{bmatrix}
  \end{align*}
  }
  \only<3>{
    \begin{align*}
                       &= \sqrt{2} \bra{\psi}
                         \begin{bmatrix}
                           -1 & 0 & 0 & -1 \\
                           0 & 1 & -1 & 0 \\
                           0 & -1 & 1 & 0 \\
                           -1 & 0 & 0 & -1
                          \end{bmatrix} \ket{\psi}
    \end{align*}
    }
    \only<4>{
    \begin{align*}
                       &= \frac{1}{\sqrt{2}} (\bra{01} - \bra{10})
                         \begin{bmatrix}
                           -1 & 0 & 0 & -1 \\
                           0 & 1 & -1 & 0 \\
                           0 & -1 & 1 & 0 \\
                           -1 & 0 & 0 & -1
                          \end{bmatrix} (\ket{01} - \ket{10})
    \end{align*}
    }
    \only<5>{
    \begin{align*}
                       &= \frac{1}{\sqrt{2}}
                         \begin{bNiceArray}{CCCC}[first-col, first-row]
                           & & 1 & -1 & \\
                           & -1 & 0 & 0 & -1 \\
                           1 & 0 & 1 & -1 & 0 \\
                           -1 & 0 & -1 & 1 & 0 \\
                           & -1 & 0 & 0 & -1
                          \end{bNiceArray} = \frac{4}{\sqrt{2}} = 2 \sqrt{2}
    \end{align*}
    }

\end{frame}

\begin{frame}{Learn more?}
  \begin{itemize}
    \item Q\# is a programming language where you can try simulating it.
    \item There are other quantum weirdness. I would love to see
          a talk on the Kochen–Specker theorem.
    \item There are slightly different Quantum Logic foundations that
          involve lattices of events (projections).
  \end{itemize}
\end{frame}

\begin{frame}{Other interesting things}
  \begin{itemize}
    \item Non-commutative probability in general
    \item Partial traces
    \item Connection to ``free probability'' and non-commutative probability
  \end{itemize}
  Things which I am not really qualified to talk about, but would be cool talks.
  \begin{itemize}
    \item Mathematics of decoherence in random subsystems
    \item Real quantum computing talk like about the quantum Fourier transform.
  \end{itemize}
\end{frame}

\begin{frame}[t, allowframebreaks]{References}
  \nocite{*}
\bibliographystyle{IEEEtran}
\bibliography{bellstheorem}
\end{frame}

\end{document}

% Local Variables:
% TeX-engine: default
% TeX-command-extra-options: "-shell-escape"
% End:
