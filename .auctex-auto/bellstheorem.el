(TeX-add-style-hook
 "bellstheorem"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "lug"
    "lug10"
    "tikz-cd"
    "parskip"
    "bbm"
    "nicematrix"
    "physics")
   (TeX-add-symbols
    '("ceil" 1)
    '("floor" 1)
    '("Div" 1)
    '("Mod" 1)
    "Q"
    "R"
    "Z"
    "C"
    "sep"
    "dif"
    "M"
    "E"
    "lcm"
    "Inn"
    "id"
    "gitlocation")
   (LaTeX-add-bibliographies))
 :latex)

